package com.pintask;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pintask.http.WebPost;
import com.pintask.util.Validator;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PARAM_AUTHTOKEN_TYPE = "auth.token";
    public static final String PARAM_CREATE = "create";

    private EditText etPhone, etCode;

    private TextView tvToken, tvPin;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etCode = (EditText) findViewById(R.id.et_code);
        etPhone = (EditText) findViewById(R.id.et_phone);

        tvPin = (TextView) findViewById(R.id.pin);
        tvToken = (TextView) findViewById(R.id.token);

        findViewById(R.id.btn_submit).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (Validator.isEmpty(etCode)) {
            Toast.makeText(MainActivity.this, "Please type international code", Toast.LENGTH_SHORT).show();
        } else if (Validator.isEmpty(etPhone)) {
            Toast.makeText(MainActivity.this, "Please type mobile number", Toast.LENGTH_SHORT).show();
        } else {
            String url = "http://bebetrack.com/api/create";

            try {
                JSONObject object = new JSONObject();
                object.put("phone", etPhone.getText().toString());
                object.put("internationalCode", etCode.getText().toString());
                mDialog = ProgressDialog.show(this, "", "Please wait....");
                new WebPost() {

                    @Override
                    protected void onPostExecute(String result) {
                        super.onPostExecute(result);
                        if (mDialog != null)
                            mDialog.dismiss();
                        if (result == null) {
                            Toast.makeText(MainActivity.this, "Please try again, internet error", Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                String token = jsonObject.optString("token");
                                String pin = jsonObject.optString("pin");
                                tvToken.setText(token);
                                tvPin.setText(pin);
                                addAccount(token, pin);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }.execute(new String[]{url, object.toString()});
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void addAccount(String token, String pin) {
        AccountManager accountManager = AccountManager.get(this.getApplicationContext());
        Account account = new Account("bebetrack", "bebetrack.com");
        accountManager.addAccountExplicitly(account, pin, null);
        accountManager.setAuthToken(account, "bebetrack.com", token);

        Toast.makeText(MainActivity.this, "Account added successfully", Toast.LENGTH_SHORT).show();
    }
}
