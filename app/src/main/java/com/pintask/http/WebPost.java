package com.pintask.http;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 */
public abstract class WebPost extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... params) {
        StringBuilder builder = new StringBuilder();
        try {
            HttpClient client = getHttpClient();

            HttpPost httppost = new HttpPost(params[0]);
            String requestParam = params[1];
            Log.d("ApiRequest", requestParam);
            StringEntity se = new StringEntity(requestParam);
            httppost.addHeader("Accept", "application/json");
            httppost.addHeader("Content-type", "application/json");

            httppost.setEntity(se);
            HttpResponse response = client.execute(httppost);
            HttpEntity entity = response.getEntity();
            InputStream content = entity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(content));
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }

            String result = builder.toString();
            Log.d("ApiResponse", result);

            return result;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            if (e != null)
                e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }

    /**
     * get DefaultHttpClient
     *
     * @return DefaultHttpClient
     */

    public static DefaultHttpClient getHttpClient() {
        HttpParams params = new BasicHttpParams();
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(params, "UTF-8");
        HttpConnectionParams.setStaleCheckingEnabled(params, false);
        HttpConnectionParams.setConnectionTimeout(params, 10 * 1000);
        HttpConnectionParams.setSoTimeout(params, 10 * 1000);
        HttpConnectionParams.setSocketBufferSize(params, 1024 * 10);
        HttpClientParams.setRedirecting(params, false);
        return new DefaultHttpClient(params);
    }
}
